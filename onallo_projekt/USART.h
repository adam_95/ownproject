﻿/*
 * USART.h
 *
 * Created: 2017. 12. 03. 11:43:40
 *  Author: Ádám
 */ 


#ifndef USART_H_
#define USART_H_


#include <avr/io.h>


void USART_init( unsigned int ubrr);
void USART_Transmit( unsigned char data );
extern unsigned char USART_Receive( void );
void USART_puts0(const char* str);




#endif /* USART_H_ */