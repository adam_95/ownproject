﻿/*
 * adc.c
 *
 * Created: 2017. 12. 03. 10:42:11
 *  Author: Ádám
 */ 


#include "adc.h"


void AD_init(void)
{
	ADMUX |= (1 << REFS0);		//referenciafeszültség, belső (ez biztonságos)
	ADCSRA |= (1 << ADEN);		//AD konverter engedélyezve van
	ADCSRA |= (1 << ADPS2);		//16os elősztás hogy pontosabb legyen az átalakítás	
}

void AD_convert(uint8_t chanel, uint16_t *eredmeny)
{
	chanel = chanel & 0x07;
	ADMUX |= chanel;			//csatornát kiválasztjuk, erről vesszük a jelet
	ADCSRA |= (1 << ADSC);	//konverzió elindítása
	while (!(ADCSRA & (1 << ADIF)));			//ez a ciklus addig fut amíg le nem fut a teljes konverzió
	*eredmeny = ADC;		//alsó 10 bit kerül az eredménybe
	ADMUX &= ~chanel;		//szabaddá tesszük a csatornát
	
}