﻿/*
 * adc.h
 *
 * Created: 2017. 12. 03. 10:42:26
 *  Author: Ádám
 */ 


#ifndef ADC_H_
#define ADC_H_

#include <avr/io.h>

void AD_init(void);
extern void AD_convert(uint8_t chanel, uint16_t *eredmeny);



#endif /* ADC_H_ */