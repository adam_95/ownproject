﻿/*
 * timer.c
 *
 * Created: 2017. 12. 03. 11:05:13
 *  Author: Ádám
 */ 

#include "timer.h"

void MINTAVET(void)
{
	TCCR0A |= (1 << WGM01);		//CTC üzemmód bekapcs
	TCCR0B |= (1 << CS02)|(1 << CS00);			// 1024 es előosztás
	TCNT0 = 0x00;
	OCR0A = 0x4E;								//5ms onként hívódik meg
	TIMSK0 |= (1 << OCIE0A);
	sei();						//lehet hogy nem ide kell, de egyenlőre itt engedélyezzük hogy mehessen a timer
}

