/*
 * AD.c
 *
 * Created: 2017. 12. 03. 10:39:50
 * Author : Ádám
 */ 

#include <avr/io.h>

#define FOSC 16000000 // Clock Speed
#define BAUD 9600
#define MYUBRR FOSC/16/BAUD-1

#define  F_CPU 16000000UL



#include "timer.h"
#include "adc.h"
#include "USART.h"




volatile uint16_t nyomas = 0;



int main(void)
{
	USART_init(MYUBRR);
	
	MINTAVET();
	AD_init();
	
	
	
    while (1) 
    {
	
		
    }
}




ISR(TIMER0_COMPA_vect)
{
	
	float vegertek;
	
	char kiir[15] = {000};
	
	
	AD_convert(0,&nyomas);		//nyomas valtozóba került az ad átalakítás eredménye	
	vegertek=(((float)nyomas*5.00)/1024)*1000;		//5 mert ez a legnagyobb feszültsé ami van ebbe a bigybaszba
	
		
	nyomas = vegertek;

	itoa(nyomas,kiir,10);
		
	USART_puts0(kiir);
		
}

